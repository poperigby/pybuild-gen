# Pybuild Gen
Generates a .pybuild template (for Portmod) with the specified name and version.

## Instructions
Just run `pybuild-gen <name of mod> <version>` and it will create a bare bones `.pybuild` for you. You can also use the `-n` switch to make it a `NexusMod`.
