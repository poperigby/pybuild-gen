import argparse
import datetime


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n", "--nexus", help="add Nexus Mods options", action="store_true"
    )
    parser.add_argument("name")
    parser.add_argument("version")
    args = parser.parse_args()
    gen_pybuild(args.name, args.version, args.nexus)


def gen_pybuild(name: str, version: str, nexus: bool = False):
    year = datetime.datetime.now().date().year

    if nexus:
        with open(f"{name}-{version}.pybuild", "w") as file:
            file.write(
                f"""# Copyright {year} Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import Pybuild1, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = ""
    DESC = ""
    HOMEPAGE = ""
    NEXUS_URL = HOMEPAGE
    LICENSE = ""
    KEYWORDS = ""
    RDEPEND = ""
    IUSE = ""
    SRC_URI = ""
    INSTALL_DIRS = []
                """
            )
        file.close()
    else:
        with open(f"{name}-{version}.pybuild", "w") as file:
            file.write(
                f"""# Copyright {year} Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import Pybuild1, InstallDir


class Package(Pybuild1):
    NAME = ""
    DESC = ""
    HOMEPAGE = ""
    LICENSE = ""
    KEYWORDS = ""
    RDEPEND = ""
    IUSE = ""
    SRC_URI = ""
    INSTALL_DIRS = []
                """
            )
        file.close()


get_args()
